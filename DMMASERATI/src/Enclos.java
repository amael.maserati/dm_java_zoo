import java.util.ArrayList;
import java.util.List;

public class Enclos {
    /** 
     * cet attribut modélise le nom de l'enclos 
    */ 
    private String nom;
    /**
     * cet attribut modélise la superficie de l'enclos
    */
    private double superficie;
    /**
     * cet attribut modélise la liste des animaux de l'enclos
    */
    private List<Animal> animauxDeLEnclos;
    /**
     * permet de créer un enclos
     * @param nom le nom de l'enclos
     * @param superficie la superficie de l'enclos
    */
    public Enclos (String nom, double superficie) {
        this.nom = nom;
        this.superficie = superficie;
        this.animauxDeLEnclos = new ArrayList<>();
    }
    /**
     * permet de récupérer le nom de l'enclos
     * @return le nom de l'enclos
    */
    public String getNom() {
        return this.nom;
    }
    /**
     * permet de récupérer la superficie de l'enclos
    */
    public double getSuperficie() {
        return this.superficie;
    }
    /**
     * permet de changer la superficie de l'enclos si on souhaite l'agrandir ou le rétrécir
     * @param superficie la superficie de l'enclos souhaitée pour le changement
    */
    public void setSuperficie(double superficie) {
        this.superficie = superficie;
    }
    /**
     * permet de savoir si un animal peut être ajouté dans l'enclos
     * @param animal l'animal à ajouter dans l'enclos
     * @return true si l'animal est ajoutable dans l'enclos, false sinon
    */
    public boolean estAjoutableDansLEnclos(Animal animal) {
        if (this.animauxDeLEnclos.isEmpty()) { // Si la liste des animaux de l'enclos est vide
            return true; // L'animal est ajoutable dans l'enclos car il n'y a pas encore de type d'animal défini dans l'enclos
        }
        if (!this.animauxDeLEnclos.isEmpty() && this.animauxDeLEnclos.get(0).getClass().equals(animal.getClass())) { // Si la liste des animaux de l'enclos n'est pas vide et que le premier animal de la liste est de la même classe que l'animal à ajouter dans l'enclos
            return true; // L'animal est ajoutable dans l'enclos
        }
        return false; // Sinon, il ne l'est pas
    }
    /**
     * permet d'ajouter un animal dans l'enclos
     * @param animal l'animal à ajouter dans l'enclos
    */
    public void ajouterAnimalDansLEnclos(Animal animal) {
        if (this.estAjoutableDansLEnclos(animal)) { // On vérifie si l'animal est ajoutable dans l'enclos
            this.animauxDeLEnclos.add(animal);
        }    
    }
    /**
     * permet de retirer un animal de l'enclos
     * @param animal l'animal à retirer de l'enclos
    */
    public void retirerAnimalDeLEnclos(Animal animal) {
        if (this.animauxDeLEnclos.contains(animal)) { // On vérifie si l'animal est présent dans l'enclos
            this.animauxDeLEnclos.remove(animal);
        }
    }
    /**
     * permet d'obtenir la liste des animaux de l'enclos
     * @return la liste des animaux de l'enclos
    */
    public List<Animal> animauxDeLEnclos() {
        return this.animauxDeLEnclos;
    }
}
