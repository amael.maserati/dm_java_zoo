public class Serpent extends Animal {
    /** 
     * cet attribut modélise si le serpent est venimeux ou non
    */
    private boolean estVenimeux;
    /**
     * permet de créer un serpent
     * @param nom le nom du serpent
     * @param poids le poids du serpent
     * @param estBlesse true si le serpent est blessé, false sinon
     * @param estVenimeux true si le serpent est venimeux, false sinon
    */
    public Serpent(String nom, double poids, boolean estBlesse, boolean estVenimeux) {
        super(nom, poids, estBlesse);
        this.estVenimeux = estVenimeux;
    }
    /**
     * permet de savoir si le serpent est venimeux ou non
     * @return true si le serpent est venimeux, false sinon
    */
    public boolean estVenimeux() {
        return this.estVenimeux;
    }
    /**
     * permet au serpent de siffler
    */
    public void emettreSon() {
        System.out.println("sssssSSSSssss");
    }
    /**
     * permet d'afficher les informations du serpent
     * @return les informations du serpent
    */
    @Override
    public String toString() {
        String venimeux = this.estVenimeux ? " et est venimeux" : " et n'est pas de venimeux"; 
        return super.toString() + venimeux;
    }
}
