import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Zoo {
    /** 
     * cet attribut modélise le nom du zoo */
    private String nom;
    /** 
     * cet attribut modélise la liste des enclos du zoo
     */
    private List<Enclos> lesEnclos;
    /**
     * cet attribut modélise la liste des animaux du zoo
     */
    /**
     * cet attribut modélise la liste des animaux du zoo
     */
    private List<Animal> animauxDuZoo;
    /**
     * permet de créer un zoo
     * @param nom le nom du zoo
     */
    public Zoo(String nom) {
        this.nom = nom;
        this.lesEnclos = new ArrayList<>();
        this.animauxDuZoo = new ArrayList<>();
    }
    /**
     * permet de récupérer le nom du zoo
     * @return le nom du zoo
     */
    public String getNom() {
        return this.nom;
    }
    /**
     * permet d'ajouter un animal dans le zoo
     * @param animal un animal
     */
    public void ajouteAnimal(Animal animal) {
        this.animauxDuZoo.add(animal); 
    }
    /**
     * permet d'ajouter un enclos dans le zoo
     * @param enclos un enclos
     */
    public void ajouteEnclos(Enclos enclos) {
        this.lesEnclos.add(enclos);
    }
    /**
     * permet de soigner un animal
     * @param nomAnimal le nom de l'animal à soigner
     * @throws AnimalPasBlesseException relève une exception si l'animal n'est pas blessé
     * @throws AnimalPasDansZooException relève une exception si l'animal n'est pas dans le zoo
     */
    public void soigner(String nomAnimal) throws AnimalPasBlesseException , AnimalPasDansZooException{
        Animal animal = null; // On initialise l'animal à null et on cherche l'animal dans la liste des animaux du zoo
        for (Animal a : this.animauxDuZoo) { // On parcourt la liste des animaux du zoo
            if (a.getNom().equals(nomAnimal)) { // Si un animal a le même nom que celui placé en paramètre
                animal = a; // On affecte cet élément de liste à la valeur animal
            }
        }
        try {
            if (animal == null) { // Si l'animal n'est pas dans la liste des animaux du zoo, on relève une exception
                throw new AnimalPasDansZooException();
            }
            else if (!animal.estBlesse()) { // Si l'animal n'est pas blessé, on relève une exception
                throw new AnimalPasBlesseException();
            }
            else { // Sinon, on soigne l'animal, donc son attribut estBlesse prend la valeur false
                animal.setEstBlesse(false);
                System.out.println("L'animal " + nomAnimal + " a été soigné");
            }
        }
        // On attrape les exceptions et on affiche un message
        catch (AnimalPasBlesseException e) {
            System.out.println("L'animal " + nomAnimal + " n'est pas blessé");
        }
        catch (AnimalPasDansZooException e) {
            System.out.println("L'animal " + nomAnimal + " n'est pas dans le zoo");
        }
    }
    /**
     * permet de récupérer la liste des animaux dans un enclos
     * @param nomEnclos nom de l'enclos
     * @return la liste des animaux dans l'enclos
     * @throws NomEnclosException relève une exception si l'enclos n'existe pas
     */
    public List<Animal> listerAnimauxDansEnclos(String nomEnclos) throws NomEnclosException{
        Enclos enclos = null; // On initialise l'enclos à null et on cherche l'enclos dans la liste des enclos du zoo
        for (Enclos e : this.lesEnclos) { // On parcourt la liste des enclos du zoo
            if (e.getNom().equals(nomEnclos)) { // Si un enclos a le même nom que celui placé en paramètre
                enclos = e; // On affecte cet élément de liste à la valeur enclos
            }
        }
        try {
            if (enclos == null) {
                throw new NomEnclosException(); // Si l'enclos n'est pas dans la liste des enclos du zoo, on relève une exception
            }
            else {
                return enclos.animauxDeLEnclos(); // Sinon, on retourne la liste des animaux de l'enclos
            }
        }
        // On attrape l'exception et on affiche un message
        catch (NomEnclosException e) {
            System.out.println(nomEnclos + " n'existe pas");
        }
        return null;
    }
    /**
     * permet de trier les animaux du zoo par ordre alphabétique
     */
    public void trierAnimauxParNom() { 
        Collections.sort(this.animauxDuZoo, new ComparateurNomAnimaux());
    }
    /**
     * permet de récupérer l'animal le plus lourd du zoo
     * @return l'animal le plus lourd du zoo
     */
    public Animal animalLePlusLourd() {
        List<Animal> listeAnimauxTriee = new ArrayList<>(this.animauxDuZoo);
        Collections.sort(listeAnimauxTriee, new ComparateurPoidsAnimaux());
        return listeAnimauxTriee.get(0); // On récupère l'indice 0 de la liste triée car le comparateur trie de manière décroissante par poids
    }
    /**
     * permet de récupérer la liste des animaux du zoo
     */
    public List<Animal> lesAnimauxDansZoo() {
        return this.animauxDuZoo;
    }
    /**
     * permet d'afficher les informations du zoo avec les noms d'enclos et les animaux qu'ils contiennent sous forme de liste
     */
    @Override
    public String toString() {
        String chaine = "Le zoo " + this.nom + " contient\n[";
        for (Enclos e : this.lesEnclos) {
            for (Animal a : e.animauxDeLEnclos()) {
                if (a instanceof Lion) { // Si l'animal est un lion
                    Lion l = (Lion) a; // On caste l'animal en lion pour utiliser la méthode toString() de la classe Lion
                        chaine += "dans " + e.getNom() + " (" + String.valueOf(e.getSuperficie()) +" m2) " + l.toString() + ",\n";
                }
                else if (a instanceof Serpent) { // Si l'animal est une serpent
                    Serpent s = (Serpent) a; // On caste l'animal en serpent pour utiliser la méthode toString() de la classe Serpent
                        chaine += "dans " + e.getNom() + " (" + String.valueOf(e.getSuperficie()) +" m2) " + s.toString() + ",\n";
                }
            }
        }
        return chaine.substring(0, chaine.length()-2) + "\n]"; // On utilise la méthode subString() pour enlever la dernière virgule de la chaine 
    }
}