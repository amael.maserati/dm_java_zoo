import java.util.Comparator;

public class ComparateurNomAnimaux implements Comparator<Animal>{ 
    /**
     * permet de comparer deux animaux en fonction de leur nom
     * @param a1 un animal
     * @param a2 un autre animal
     * @return 0 si les deux animaux ont le même nom, un nombre négatif si le nom de l'animal a1 est inférieur à celui d'a2 ou un nombre positif si le nom de l'animal a1 est supérieur à celui d'a2
     */
    @Override
    public int compare(Animal a1, Animal a2) {
        return a1.getNom().compareTo(a2.getNom());
    }
}
