public class AnimalPasBlesseException extends Exception {
    public AnimalPasBlesseException() {
        super("L'animal n'est pas blessé");
    }
}
