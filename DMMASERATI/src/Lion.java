public class Lion extends Animal{
    /**
     * cet attribut modélise si le lion possède une crinière ou non
    */
    private boolean aUneCriniere;
    /**
     * permet de créer un lion
     * @param nom le nom du lion
     * @param poids le poids du lion
     * @param estBlesse true si le lion est blessé, false sinon
     * @param aUneCriniere true si le lion possède une crinière, false sinon
    */
    public Lion (String nom, double poids, boolean estBlesse, boolean aUneCriniere) {
        super(nom, poids, estBlesse);
        this.aUneCriniere = aUneCriniere;
    }
    /**
     * permet de savoir si le lion possède une crinière ou non
     * @return true si le lion possède une crinière, false sinon
    */
    public boolean possedeUneCriniere() {
        return this.aUneCriniere;
    }
    /**
     * permet au lion de rugir
    */
    public void emettreSon() {
        System.out.println("Roooooar");
    }
    /**
     * permet d'afficher les informations du lion
     * @return les informations du lion
    */
    @Override
    public String toString() {
        String criniere = this.aUneCriniere ? " et possède une crinière" : " et n'a pas de crinière"; 
        return super.toString() + criniere;
    }
}
