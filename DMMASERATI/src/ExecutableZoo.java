import java.util.Arrays;

public class ExecutableZoo {
    public static void main(String [] args) throws AnimalPasBlesseException, AnimalPasDansZooException, NomEnclosException{
        // Création du zoo
        Zoo beauval = new Zoo("Beauval");

        // Création et ajout des enclos au zoo
        Enclos enclosSerpents = new Enclos("l'enclos des serpents", 100);
        Enclos enclosLions = new Enclos("l'enclos des lions", 200);
        beauval.ajouteEnclos(enclosSerpents);
        beauval.ajouteEnclos(enclosLions);
        
        // Création et ajout des animaux au zoo
        Lion simba = new Lion("Simba", 190, false, true);
        Lion nala = new Lion("Nala", 130, true, false);
        Serpent nagini = new Serpent("Nagini", 30, true, true);
        Serpent vipere = new Serpent("Vipere", 15, false, false);
        beauval.ajouteAnimal(simba);
        beauval.ajouteAnimal(nala);
        beauval.ajouteAnimal(nagini);
        beauval.ajouteAnimal(vipere);

        // Ajout des animaux dans les enclos
        enclosLions.ajouterAnimalDansLEnclos(simba);
        enclosLions.ajouterAnimalDansLEnclos(nala);
        enclosSerpents.ajouterAnimalDansLEnclos(nagini);
        enclosSerpents.ajouterAnimalDansLEnclos(vipere);

        // tests pour la classe Zoo
        assert beauval.getNom().equals("Beauval");
        assert beauval.animalLePlusLourd().equals(simba);
        beauval.trierAnimauxParNom();
        assert beauval.lesAnimauxDansZoo().equals(Arrays.asList(nagini, nala, simba, vipere));
        assert beauval.listerAnimauxDansEnclos("l'enclos des serpents").equals(Arrays.asList(nagini, vipere));
        assert beauval.lesAnimauxDansZoo().equals(Arrays.asList(nagini, nala, simba, vipere));
        System.out.println(beauval.toString()); 
        System.out.println(" ");

        // tests pour la classe Enclos
        assert enclosSerpents.getNom().equals("l'enclos des serpents");
        assert enclosLions.getSuperficie() == 200;
        assert !enclosSerpents.estAjoutableDansLEnclos(simba);
        assert enclosLions.estAjoutableDansLEnclos(nala);
        assert enclosSerpents.animauxDeLEnclos().size() == 2;
        assert !enclosLions.estAjoutableDansLEnclos(vipere);
        assert enclosSerpents.estAjoutableDansLEnclos(nagini);

        // tests pour la classe Animal
        assert simba.getNom().equals("Simba");
        assert nala.getPoids() == 130;
        assert !simba.estBlesse();
        assert nala.estBlesse();
        System.out.println("Soignement de Nagini : ");
        beauval.soigner("Nagini"); // Soignement d'un animal blessé
        assert !nagini.estBlesse();

        // tests pour la classe Lion
        assert simba.possedeUneCriniere();
        assert !nala.possedeUneCriniere();

        // tests pour la classe Serpent
        assert nagini.estVenimeux();
        assert !vipere.estVenimeux();

        // affichages pour les exceptions
        System.out.println(" ");
        System.out.println("Soignement de Simba : ");
        beauval.soigner("Simba"); // Soignement d'un animal non blessé
        System.out.println(" ");
        beauval.listerAnimauxDansEnclos("l'enclos des singes"); // Listage des animaux d'un enclos inexistant
        System.out.println(" ");
        beauval.soigner("Mufasa"); // Soignement d'un animal inexistant

        // sons des animaux
        System.out.println(" ");
        System.out.println("Simba rugit");
        simba.emettreSon();
        System.out.println(" ");
        System.out.println("Nagini siffle");
        vipere.emettreSon();
    }
}
