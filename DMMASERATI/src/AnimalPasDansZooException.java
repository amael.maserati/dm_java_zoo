public class AnimalPasDansZooException extends Exception {
    public AnimalPasDansZooException() {
        super("L'animal n'est pas dans le zoo");
    }
}
