public class Animal {
    // cet attribut modélise le nom de l'animal
    private String nom;
    // cet attribut modélise le poids de l'animal
    private double poids;
    // cet attribut modélise si l'animal est blessé ou non
    private boolean estBlesse;
    /**
     * permet de créer un animal
     * @param nom le nom de l'animal
     * @param poids le poids de l'animal
     * @param estBlesse true si l'animal est blessé, false sinon
    */
    public Animal(String nom, double poids, boolean estBlesse) {
        this.nom = nom;
        this.poids = poids;
        this.estBlesse = estBlesse;
    }
    /**
     * permet d'obtenir le nom de l'animal
     * @return le nom de l'animal
    */
    public String getNom() {
        return this.nom;
    }
    /**
     * permet d'obtenir le poids de l'animal
     * @return le poids de l'animal
    */
    public double getPoids() {
        return this.poids;
    }
    /**
     * permet de savoir si l'animal est blessé ou non
     * @return true si l'animal est blessé, false sinon
    */
    public boolean estBlesse() {
        return this.estBlesse;
    }
    /**
     * permet de changer la valeur de l'attribut estBlesse de l'animal s'il a été blessé ou s'il a été soigné 
     * @param estBlesse
    */
    public void setEstBlesse(boolean estBlesse) {
        this.estBlesse = estBlesse;
    }
    /**
     * permet d'afficher les informations de l'animal
     * @return les informations de l'animal
    */
    @Override
    public String toString() {
        String blessure = estBlesse() ? ", blessé," : ", non blessé,";
        return this.nom + blessure + " pèse " + String.valueOf(this.poids) + " kg";
    }
}