import java.util.Comparator;

public class ComparateurPoidsAnimaux implements Comparator<Animal> { 
    /**
     * permet de comparer deux animaux en fonction de leur poids
     * @param a1 un animal
     * @param a2 un autre animal
     * @return 0 si les deux animaux ont le même poids, un nombre négatif si le poids de l'animal a2 est supérieur à celui d'a2 ou un nombre positif si le poids de l'animal a2 est inférieur à celui d'a2
     */
    @Override
    public int compare(Animal a1, Animal a2) {
        return Double.compare(a2.getPoids(), a1.getPoids());
    }
}
